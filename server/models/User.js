

const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

let UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        trim: true,
        minlength: 1,
        unique: true,
        validate: {
            validator: validator.isEmail,
            message: '{VALUE} is not a valid email'
        }
    },
    password: {
        type: String,
        require: true,
        minlength: 6
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]
});

UserSchema.methods.toJSON = function () {
    const user = this;
    const userObject = user.toObject();

    return _.pick(userObject, ['_id', 'email']);
};

UserSchema.methods.generateAuthToken = function () {  // .methods to create an instance method
    const user = this;
    const access = 'auth';
    let token = jwt.sign({_id: user._id.toHexString(), access}, 'abc123').toString();

    user.tokens.push({access, token});

    return user.save().then(() => {
        return token;
    });
};

UserSchema.statics.findByToken = function (token) { // .statics to create a model method
     const User = this;
    let decoded;

 try {
   decoded = jwt.verify(token, 'abc123');
 } catch (e) {
    // return new Promise((res, rej) => {
    //     rej();
    // });

     return  Promise.reject();
 }
   return User.findOne({
       '_id': decoded._id,
       'tokens.token': token,  // to query nested value you need to use ''
       'tokens.access': 'auth'
   });
};

UserSchema.pre('save', function (next) {   // function needs this binding and next for middleware to continue
    const user = this;
    if (user.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, (err, hash) => {
                user.password = hash;
                next();
            })
        });

    } else {
        next();
    }
});

let User = mongoose.model('User', UserSchema);

module.exports = {User};