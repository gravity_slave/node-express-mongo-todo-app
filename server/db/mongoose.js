const mongoose = require('mongoose');
// set up for using promises
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI);

module.exports = { mongoose};

// const Todo = mongoose.model('Todo', {
//     text: {
//         type: String,
//         required: true,
//         minlength: 1
//     },
//
//     completed: {
//         type: Boolean,
//         default: false
//     },
//
//     completedAt: {
//         type: Number,
//         default: null
//     }
// });
//
// const User =mongoose.model('User', {
//     email: {
//         type: String,
//         required: true,
//         trim: true,
//         minlength: 1
//     }
// });
//
// const newUSer = new User({
//     email: 'asdf@asdf.com'
// });
//
// newUSer.save().then(data => {
//     console.log('New user',data);
// }, err => {
//     console.log('Error required',err);
// });

// const newTodo = new Todo({
//     text: 'Cook dinner'
//
// });
//
// newTodo.save().then(saved => {
//     console.log('Saved Todo',saved);
// }, err => {
//     console.log(err);
// });

// const otherTodo = new Todo({
//     text: 'Edit the video'
//     // completed: true,
//     // completedAt: 11
// });
//
// otherTodo.save().then((sved) => {
//     console.log(JSON.stringify(sved,undefined,2));
// }, err => {
//    console.log(err);
// });



