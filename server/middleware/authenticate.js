const {User} = require('./../models/User');

// middleware function
const authenticate = (req, res, next) => {
    const token = req.header('x-auth'); // get the value of a header

    User.findByToken(token).then((user) => {
        if (!user) {
            return Promise.reject();
        }

        req.user= user;
        req.token = token;
        next(); //to  continue
    }).catch((err) => {
        res.status(401).send();
    });
};

module.exports = { authenticate};