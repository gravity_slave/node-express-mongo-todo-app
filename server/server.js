require('./config/config');

const express = require('express');
const bodyParser = require('body-parser');
const {ObjectID} = require('mongodb');
const _ = require('lodash');
const port = process.env.PORT;

const {authenticate} = require('./middleware/authenticate');
const {mongoose} = require('./db/mongoose');

const {User} =  require('./models/User');
const {Todo} = require('./models/Todo');

const app = express();

app.use(bodyParser.json());

app.post('/todos', (req, res) => {
    const todo = new Todo({
       text: req.body.text
    });

    todo.save().then((todos) => {
        res.send(todos);
    }, (err) => {
        res.status(400).send(err);
    });
});

app.get('/todos/:id', (req, res) => {
    const id = req.params.id;
    if(!ObjectID.isValid(id)) {
        return res.status(404).send();

    }
    Todo.findById(id).then((todos) =>{
        if (!todos) return res.status(404).send();
        res.send({todos})
    }, err => res.status(404).send(err));
});

app.get('/todos', (req, res) => {

    Todo.find({}).then((todos) => {
        res.send({
            todos
        })
    }, err => res.status(404).send(err));
});

app.delete('/todos/:id', (req, res) => {
    const id = req.params.id;

    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Todo.findByIdAndRemove(id).then((todos) => {
        if (!todos) {
            return res.status(404).send();
        }

        res.send({todos});
    }).catch((err) => {
        res.status(400).send();
    });
});

//private route with express middleware
app.patch('/todos/:id', (req, res) => {
    const id = req.params.id;
    const body = _.pick(req.body, ['text', 'completed']);
    if (!ObjectID.isValid(id))  return res.status(404).send();

    if (_.isBoolean(body.completed, true)) {
      body.completedAt = new Date().getDate();
    } else {
        body.completed = false;
        body.completedAt = null;
    }
    Todo.findByIdAndUpdate(id, {$set: body}, {new: true})
        .then( todos => {
            if (!todos) return res.status(404).send(todos);
            res.send({todos});
        })
        .catch(err => res.status(400).send());


});

//POST


app.post('/users', (req, res) => {
    const body = _.pick(req.body, ['email', 'password']);
    let user = new User(body);

    user.save().then(() => {
        return user.generateAuthToken();
    }).then((token) => {
        res.header('x-auth', token).send(user); //set  a header
    }).catch((e) => {
        res.status(400).send(e);
    })
});




app.get('/users/me', authenticate ,(req, res) => {
        res.send(req.user);
});

if(!module.parent){
    app.listen(port,  () => {
        console.log('THe server is running on port ' + port);
    });
}



module.exports = {app};
